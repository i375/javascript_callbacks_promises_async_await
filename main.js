function main() {
    //promisesSample()

    asyncAwaitSample()
}

// Contains Promise sample using then/catch
function promisesSample() {

    var promise1 = promisedTimeout()
    //                                 This `then` function will be called when `promise1` resolves
    //                                  ↓
    var promise2 = promise1.then((_msg) => {
        console.log(`PROMISE_SAMPLE: Then 1 \n\tMSG: ${_msg}`)

        return Promise.resolve(123)
    })
    //                        This `then` function will be called when `promise2` resolves
    //                         ↓
    promise2.then((_numberMsg) => {
        console.log(`PROMISE_SAMPLE: Then 2 \n\tMSG: ${_numberMsg}`)

        return promisedTimeout() //      ←────────────────────┐
    })  //                                                    │
    //           ┌ This `then` function will be called when above `promisedTimeout` `Promise` resolves
    //           ↓
    .then((_msg) => {
        console.log(`PROMISE_SAMPLE: Then 3 \n\tMSG: ${_msg}`)
    })
    // If any of the above `Promises` fails (gets `rejected`)
    //               this callback will be called, receiveing error mesasge send during rejection
    .catch((_errorMsg) => {
        console.log(`PROMISE_SAMPLE: Error while processing Then 1,2,3 chain. \n\tMSG: ${_errorMsg}`)
    })

}

// ↙This `async` marks function as async so that it can make calls to asynchronous functions and await their Promise resolusions
async function asyncAwaitSample() {
    try {
        // var promisedTimeout = promisedTimeout()                                               ←────┐
        // promisedTimeout().then((awaitedMsg1)=>{                                                    │
        //   console.log(`ASYNC_AWAIT_SAMPLE: \n\tawaited Msg1: ${awaitedMsg1}`)                      │
        //   return Promise.resolve(123)                                                              │
        // })                                                                                         │
        // .then((numberMsg2)=>{                                                                      │
        //   onsole.log(`ASYNC_AWAIT_SAMPLE: \n\tawaited Msg2: ${numberMsg2}`)                        │
        // })                                                                                    ←────┤
        //                                                                                            │
        //                                                                                          These have same meaning
        //                                                                                            │
        var awaitedMsg1 = await promisedTimeout()                                          // ←───────┤
        console.log(`ASYNC_AWAIT_SAMPLE: \n\tawaited Msg1: ${awaitedMsg1}`)                //         │
                                                                                           //         │
        // This code executes only after `awaitedMsg1` gets resolved and code above is done executing │
        var numberMsg2 = await Promise.resolve(123)                                         //        │
        console.log(`ASYNC_AWAIT_SAMPLE: \n\tawaited Msg2: ${numberMsg2}`)                  // ←──────┘
                                                                                            //
        // This code executes only after `awaitedMsg2` gets resolved and code above is done executing
        var awaitedMsg3 = await promisedTimeout()
        console.log(`ASYNC_AWAIT_SAMPLE: \n\tawaited Msg3: ${awaitedMsg3}`)        
        
        // Alternativelly awaits can be grouped together, ───────────────────────────────────────┐
        // after trigering tasks, ────────────────────────────────────────────────────────┐      │  
        // so that one Promise doesnt wait for other, like in above example.              │      │  
        //                                                                                │      │  
        // In that case `msgPromise1`, `msgPromise2`, and `msgPromise3` all               │      │  
        // start running in parallel, not waiting for one another.                        │      │  
        if (false) {                                                                  //  │      │  
            var msgPromise1 = promisedTimeout("AA ")                                  // ┐│      │  
            var msgPromise2 = Promise.reject(123)                                     // ├┘      │  
            var msgPromise3 = promisedTimeout("BB ")                                  // ┘       │  
                                                                                      //         │  
            console.log(`ASYNC_AWAIT_SAMPLE: \n\tawaited Msg1: ${await msgPromise1}`) // ┐       │  
            console.log(`ASYNC_AWAIT_SAMPLE: \n\tawaited Msg2: ${await msgPromise2}`) // ├───────┘  
            console.log(`ASYNC_AWAIT_SAMPLE: \n\tawaited Msg3: ${await msgPromise3}`) // ┘          
        }
        // Thought, if all Promises get rejected, while all run in parallel,
        // only first rejection will get caught, and others will be thrown as unhandleded.

    }
    // If any of above `await` fails, raised Exception get cought here
    // ↙───────────────────────────────────────────────────────────┘
    catch (asyncErrorMsg) {
        console.log(`ASYNC_AWAIT_SAMPLE: \n\tERROR: ${asyncErrorMsg}`)
    }
}

// This function returns Promise, which will be rejected or resolved after `setTimeout`
// expires and random value gets generated.
function promisedTimeout(userData) {

    return new Promise((toBeCalledOnSuccess, toBeCalledOnFail) => {

        const processTimeRequired = Math.random() * 1000 // Simulating time required to complete asynchronous task
                                                    //                       │ 
        setTimeout((e) => {                         //                       │ 
                                                    //                       │ 
            const rnd = (Math.random() * 10) | 0    //                       │                                      
                                                    //                       │                                      
            if (rnd > 5) {                          //                       └──────────────────┐                   
                const msg = `${userData || ""}RESOLVED (with value ${rnd})`         //          │                   
                toBeCalledOnSuccess(msg)                                            //          │                   
            }                                                                       //          │                   
            else {                                                                  //          │                   
                const errorMsg = `${userData || ""}REJECTED (with value, ${rnd})`   //          │                   
                toBeCalledOnFail(errorMsg)                                          //          │                   
            }                                                                       //          │                   
        }, processTimeRequired) // ←────────────────────────────────────────────────────────────┘                   

    })

}

window.addEventListener("load", function (doc, ev) {
    main()
})